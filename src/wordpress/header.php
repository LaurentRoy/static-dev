<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta content="telephone=no" name="format-detection" />
  <meta name="HandheldFriendly" content="true" />

  <meta name="msapplication-TileColor" content="#ffffff" />
  <meta name="msapplication-TileImage" content="<?=IMG?>/favicon.png" />
  <meta name="theme-color" content="#ffffff" />
  <link rel="icon" type="image/x-icon" href="<?=IMG?>/favicon.png" />
  <link rel="shortcut icon" href="<?=IMG?>/favicon.ico" />
  <meta name="facebook-domain-verification" content="04emed1hca77bb6ex2bh06cza9uibz" />

  <link rel="preload" as="script" href="<?= ROOT . '/assets/js/plugins.min.js' ?>">
  <link rel="preload" as="script" href="<?= ROOT . '/assets/js/main.min.js' ?>">


  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <?php include "components/header.php"; ?>