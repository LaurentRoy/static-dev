<?php 

  get_header();


  $posts = get_posts([
    'post_type' => 'post',
    'category' => get_the_category()[0]->slug,
    'post_status' => 'publish',
    'numberposts' => -1,
    'orderby'    => 'menu_order',
    'sort_order' => 'asc'
  ]);

  shuffle($posts);

  $posts = array_slice($posts, 0, 3);

  include "components/article.php";

  get_footer();