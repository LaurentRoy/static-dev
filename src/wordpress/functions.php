<?php
/**
 * merci functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package merci
 */

define('VERSION', '0.1');

// show_admin_bar(false);

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

add_filter( 'wp_headers', 'itsg_add_ie_edge_wp_headers' );

function itsg_add_ie_edge_wp_headers( $headers ) {
    if ( isset( $_SERVER['HTTP_USER_AGENT'] ) && ( strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE' ) !== false ) ) {
        $headers['X-UA-Compatible'] = 'IE=edge,chrome=1';
    }
    
    return $headers;
}

define("ROOT", get_stylesheet_directory_uri());

define("IMG", get_stylesheet_directory_uri().'/assets/img');

if(defined('ICL_LANGUAGE_CODE')){
	define('LANG', ICL_LANGUAGE_CODE);
}
else {
	define('LANG', 'fr');
}
function my_acf_init() {	
	acf_update_setting('google_api_key', 'AIzaSyAfHBK7lMw1xgRC_gPFbR1c2LOLJoCNG6U');
}
add_action('acf/init', 'my_acf_init');
 
add_action( 'after_setup_theme', 'theme_setup' );
function theme_setup() {
	add_theme_support( 'yoast-seo-breadcrumbs' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	register_nav_menus([
		'menu-header' => 'Header Menu',
	]);
	register_nav_menus([
		'menu-header-secondary' => 'Header Menu secondary',
	]);
	remove_action('wp_head', 'wp_generator');
}
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'is-active ';
  }
  return $classes;
}

/**
 * Enqueue scripts and styles.
 */
function theme_scripts_styles() {
	if($_SERVER["SERVER_ADDR"] == '127.0.0.1'){
		// CSS
		wp_enqueue_style( 'main', ROOT . '/assets/css/main.css',false,VERSION);
	
		// JS
		wp_enqueue_script('plugins', ROOT.'/assets/js/plugins.min.js', null,VERSION, false);
		wp_enqueue_script('main', ROOT.'/assets/js/main.js', null,VERSION, true);
	}
	else {
		// CSS
		wp_enqueue_style( 'main', ROOT . '/assets/css/main.min.css',false,VERSION);
	
		// JS
		wp_enqueue_script('plugins', ROOT.'/assets/js/plugins.min.js', null,VERSION, false);
		wp_enqueue_script('main', ROOT.'/assets/js/main.min.js', null,VERSION, false);
	}
}
add_action( 'wp_enqueue_scripts', 'theme_scripts_styles' );

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
	add_image_size( 'full-width', 1500 );
}

// CUSTOM FUNCTION THAT DISPLAY THE URL OF THE PAGE ID TRANSLATED
function href($id){
	if(!defined("ICL_LANGUAGE_CODE")){
		return get_permalink($id); // Get the URL of a translated page (current language displayed)
	}
	else {
		return get_permalink( icl_object_id($id, 'page', true) ); // Get the URL of a translated page (current language displayed)
	}
}
// Get the link of the current translated page
function switchLanguageTo($lang) {
    return apply_filters( 'wpml_permalink', get_the_permalink() , $lang );
}

// Get the link of the current translated page
function wp_img($id, $size = 'large', $attr = '') {
  return wp_get_attachment_image( $id, $size, false, $attr );
}

function is_local() {
	$localAdresses = array(
		'127.0.0.1',
		'::1'
	);
	if(in_array($_SERVER['REMOTE_ADDR'], $localAdresses)){
		return true;
	}
	return false;
}

function isMobile() {
	$useragent=$_SERVER['HTTP_USER_AGENT'];
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
	return true;
else
	return false;
}
