<?php get_header(); ?>

<main class="site-main">
<section class="section-page section-presentation">
    <div class="container">
      <div class="">

        <?php while (have_posts()) : the_post(); ?>

          <?php the_content(); ?>

        <?php endwhile; ?>

      </div>
    </div>
  </section>
</main>

<?php get_footer() ?>